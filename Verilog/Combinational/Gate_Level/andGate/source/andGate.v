module andGate(a,b,y);
    input a,b;
    output y;

    begin
        and(y,a,b);
    end
endmodule
